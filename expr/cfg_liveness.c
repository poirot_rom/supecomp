#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "elang.h"
#include "cfg.h"


void print_mapping(FILE* f, list* map){
  while(map){
    pair* elt = map->elt;
    printf("n%d: ", (int)(unsigned long)elt->fst);
    print_string_list(f, (list*)elt->snd);
    printf("\n");
    map = map->next;
  }
}

list* expr_used_vars(struct expression* e){
    switch(e->etype){
        case EVAR:
            return list_append(NULL, e->var.s);
        case EINT:
            return NULL;
        case EUNOP:
            return expr_used_vars(e->unop.e);
        case EBINOP:
            return concat(expr_used_vars(e->binop.e1), expr_used_vars(e->binop.e2));
        default:
            return NULL;
    }
}


list* live_after(node_t* n, list* map){
    list* succs = succs_node(n);
    list* live_aft = NULL;
    while(succs){
        pair* pr = list_nth(map, list_length(map) - list_nth_int(succs, 0));
        for (int i=0; i<list_length(pr->snd); i++){
            char* s = list_nth_string(pr->snd, i);
            if (!list_in_string(live_aft, s))
                live_aft = list_append_string(live_aft, s);
        }
        succs = succs->next;
    }
    return live_aft;
}

list* live_before(list* live_aft, node_t* n){
    list* live_bef = string_list_copy(live_aft);
    list* used_vars = NULL;
    switch(n->type){
        case NODE_ASSIGN: {
            live_bef = list_remove_string(live_bef, n->assign.var); // on écrit la première var
            used_vars = expr_used_vars(n->assign.e);
            break;
        }
        case NODE_PRINT: {
            used_vars = expr_used_vars(n->print.e);
            break;
        }
        case NODE_RETURN: {
            used_vars = expr_used_vars(n->ret.e);
            break;
        }
        case NODE_COND: {
            used_vars = expr_used_vars(n->cond.cond);
            break;
        }
        default:
            break;
    }
    for (int i=0; i<list_length(used_vars); i++){
        char* s = list_nth_string(used_vars, i);
        if (!list_in_string(live_bef, s))
            live_bef = list_append_string(live_bef, s);
    }
    return live_bef;
}

int new_changes;

list* liveness_graph(list* map, cfg* c){
    list* new_live_bef;
    pair* pr;
    new_changes = 0;
    while(c){
        new_live_bef = live_before(live_after(c->node, map), c->node);
        pr = list_nth(map, list_length(map) - c->id);
        if (list_length(new_live_bef) != list_length(pr->snd))
            new_changes = 1;
        else if (!list_string_incl(new_live_bef, pr->snd))
            new_changes = 1;
        else if (!list_string_incl(pr->snd, new_live_bef))
            new_changes = 1;
        pr->snd = new_live_bef;
        c = c->next;
    }
    return map;
}

list* liveness_prog(cfg_prog* p){
    list* map = NULL;
    // Initialisation de la map
    cfg* c = p->graph;
    while(c){
        pair* pr = mkpair((void*)(unsigned long)c->id, NULL);
        map = list_append(map, pr);
        c = c->next;
    }
    // On boucle jusqu'à ce qu'il n'y ait plus de changements
    do{
        map = liveness_graph(map, p->graph);
    }while(new_changes);
    return map;
}
