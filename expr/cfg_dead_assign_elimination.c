#include "datatypes.h"
#include "elang.h"
#include "elang_print.h"
#include "cfg.h"
#include "cfg_liveness.h"
#include "cfg_gen.h"
#include "globals.h"

int num_changed = 0;
int roundnr = 0;

void dead_assign_elimination_graph(list* live, cfg* c){
    list* live_aft = NULL;
    node_t* node;
    int succ;
    num_changed = 0;
    while(c){
        node = c->node;
        if(node->type == NODE_ASSIGN){
            live_aft = live_after(node, live);
            if (!list_in_string(live_aft, node->assign.var)){
                succ = list_nth_int(succs_node(node), 0);
                node->type = NODE_GOTO;
                node->goto_succ = succ;
                num_changed = 1;
            }
        }
        c = c->next;
    }
}

cfg_prog* dead_assign_elimination_prog(cfg_prog* c){
    do {
        list* live = liveness_prog(c);
        dead_assign_elimination_graph(live, c->graph);
    } while (num_changed);
    return c;
}
