#include <stdlib.h>
#include <string.h>
#include "elang.h"
#include "cfg.h"
#include "rtl.h"

rtl_op* new_rtl_op_mov(int rd, int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RMOV;
  op->mov.rd = rd;
  op->mov.rs = rs;
  return op;
}

rtl_op* new_rtl_op_label(int label){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RLABEL;
  op->label.lab = label;
  return op;
}

rtl_op* new_rtl_op_print(int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RPRINT;
  op->print.rs = rs;
  return op;
}

rtl_op* new_rtl_op_return(int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RRET;
  op->ret.rs = rs;
  return op;
}

rtl_op* new_rtl_op_goto(int succ){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RGOTO;
  op->rgoto.succ = succ;
  return op;
}

rtl_op* new_rtl_op_branch(int rs, int succtrue, int succfalse){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RBRANCH;
  op->branch.rs = rs;
  op->branch.succtrue = succtrue;
  op->branch.succfalse = succfalse;
  return op;
}

rtl_op* new_rtl_op_binop(binop_t b, int rd, int rs1, int rs2){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RBINOP;
  op->binop.binop = b;
  op->binop.rd = rd;
  op->binop.rs1 = rs1;
  op->binop.rs2 = rs2;
  return op;
}

rtl_op* new_rtl_op_unop(unop_t u, int rd, int rs){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RUNOP;
  op->unop.unop = u;
  op->unop.rd = rd;
  op->unop.rs = rs;
  return op;
}

rtl_op* new_rtl_op_imm(int rd, int imm){
  rtl_op* op = malloc(sizeof(rtl_op));
  op->type = RIMM;
  op->imm.rd = rd;
  op->imm.imm = imm;
  return op;
}

typedef struct symtable {
  struct symtable* next;
  char* var;
  int reg;
} symtable;

void free_symtable(symtable* s){
  if(s){
    free(s->var);
    free_symtable(s->next);
    free(s);
  }
}

symtable* symt = NULL;

int curreg = 0;

int new_reg(){
  return curreg++;
}

int get_reg_for_var(char* v){
     symtable* symt_loc = symt;
     while (symt_loc) {
         if (!strcmp(symt_loc->var, v))
             return symt_loc->reg;
         symt_loc = symt_loc->next;
     }
     int reg = new_reg();
     symtable* symt_new = (symtable*)malloc(sizeof(symtable));
     symt_new->var = v;
     symt_new->reg = reg;
     symt_new->next = symt;
     symt = symt_new;
     return reg;
}

typedef struct expr_compiled {
  int r;
  list* ops;
} expr_compiled;

expr_compiled* new_expr_compiled(){
  expr_compiled* ec = (expr_compiled*)malloc(sizeof(expr_compiled));
  ec->ops = NULL;
  return ec;
}

expr_compiled* rtl_ops_of_expression(struct expression* e){
    expr_compiled* ec = new_expr_compiled();
    switch (e->etype){
        case EVAR: {
            int r = get_reg_for_var(e->var.s);
            ec->r = r;
            return ec;
        }
        case EINT: {
            int r = new_reg();
            ec->r = r;
            rtl_op* op = new_rtl_op_imm(r, e->eint.i);
            ec->ops = cons(op, NULL);
            return ec;
        }
        case EUNOP: {
            expr_compiled* ec1 = rtl_ops_of_expression(e->unop.e);
            int r = new_reg();
            ec->r = r;
            rtl_op* op = new_rtl_op_unop(e->unop.unop, r, ec1->r);
            ec->ops = list_append(ec1->ops, op);
            return ec;
        }
        case EBINOP: {
            expr_compiled* ec1 = rtl_ops_of_expression(e->binop.e1);
            expr_compiled* ec2 = rtl_ops_of_expression(e->binop.e2);
            int r = new_reg();
            ec->r = r;
            rtl_op* op = new_rtl_op_binop(e->binop.binop, r, ec1->r, ec2->r);
            ec->ops = list_append(concat(ec1->ops, ec2->ops), op);
            return ec;
        }
        default:
            break;
    }
  return NULL;
}

list* rtl_ops_of_cfg_node(node_t* c){
    switch (c->type){
        case NODE_ASSIGN: {
            int rd = get_reg_for_var(c->assign.var);
            expr_compiled* ec = rtl_ops_of_expression(c->assign.e);
            rtl_op* op = new_rtl_op_mov(rd, ec->r);
            rtl_op* op_goto = new_rtl_op_goto(c->assign.succ);
            return list_append(list_append(ec->ops, op), op_goto);
        }
        case NODE_PRINT: {
            expr_compiled* ec = rtl_ops_of_expression(c->print.e);
            rtl_op* op = new_rtl_op_print(ec->r);
            rtl_op* op_goto = new_rtl_op_goto(c->print.succ);
            return list_append(list_append(ec->ops, op), op_goto);
        }
        case NODE_RETURN: {
            expr_compiled* ec = rtl_ops_of_expression(c->ret.e);
            rtl_op* op = new_rtl_op_return(ec->r);
            return list_append(ec->ops, op);
        }
        case NODE_GOTO: {
            rtl_op* op = new_rtl_op_goto(c->goto_succ);
            return cons(op, NULL);
        }
        case NODE_COND: {
            expr_compiled* ec = rtl_ops_of_expression(c->cond.cond);
            rtl_op* op = new_rtl_op_branch(ec->r, c->cond.succ1, c->cond.succ2);
            return list_append(ec->ops, op);
        }
        default:
            break;
    }
    return NULL;
}

rtl* rtl_of_cfg_graph(cfg* c){
    if (c) {
        rtl* rtl = (struct rtl*)malloc(sizeof(struct rtl));
        rtl->id = c->id;
        rtl->next = rtl_of_cfg_graph(c->next);
        rtl->ops = rtl_ops_of_cfg_node(c->node);
        return rtl;
    }
    return NULL;
}

rtl_prog* rtl_of_cfg_prog(cfg_prog* cfg){
  struct rtl_prog* rtl = (struct rtl_prog*)malloc(sizeof(struct rtl_prog));
  rtl->fname = strdup(cfg->fname);
  rtl->args = NULL;
  list* args = cfg->args;
  int r;
  while (args) {
      r = get_reg_for_var(args->elt);
      rtl->args = list_append_int(rtl->args, r);
      args = args->next;
  }
  rtl->graph = rtl_of_cfg_graph(cfg->graph);
  rtl->entry = rtl->graph->id;
  //free_symtable(symt);
  return rtl;
}


void free_rtl_op(rtl_op* n){
  if(!n) return;
  free(n);
}

void free_rtl_ops(list* l){
  if(!l) return;
  free_rtl_ops(l->next);
  free_rtl_op(l->elt);
}

void free_rtl_graph(rtl* r){
  if(r){
    free_rtl_graph(r->next);
    free_rtl_ops(r->ops);
    //free_list(r->ops);
    free(r);
  }
}

void free_rtl(rtl_prog* rtl){
  if(rtl){
    free(rtl->fname);
    free_list(rtl->args);
    free_rtl_graph(rtl->graph);
    free(rtl);
  }
}
