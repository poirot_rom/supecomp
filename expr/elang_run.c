#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "ast.h"
#include "tree_dump.h"
#include "elang.h"
#include "state.h"

int run_unop(enum unop_t u, int e){
  switch(u){
  case ENEG:
    return 0 - e;
  default:
    return 0;
  }
}

int run_binop(enum binop_t b, int e1, int e2){
  switch(b){
  case EADD:
    return e1 + e2;
  case ESUB:
    return e1 - e2;
  case EMUL:
    return e1 * e2;
  case EDIV:
    return e1 / e2;
  case EMOD:
    return e1 % e2;
  case CEQ:
    return e1 == e2;
  case CNEQ:
    return e1 != e2;
  case CLT:
    return e1 < e2;
  case CLE:
    return e1 <= e2;
  case CGT:
    return e1 > e2;
  case CGE:
    return e1 >= e2;
  default:
    return 0;
  }
}

int run_expression(string_int_state_t* s, struct expression* e){
  switch(e->etype){
      case EINT:
          return e->eint.i;
      case EVAR:
          return string_int_get_val(s, e->var.s);
      case EUNOP:
          return run_unop(e->unop.unop, run_expression(s, e->unop.e));
      case EBINOP:
          return run_binop(e->binop.binop, run_expression(s, e->binop.e1), run_expression(s, e->binop.e2));
      default: {
          printf("run_expression: unexpected expression type: %d", e->etype);
          exit(1);
      }
  }
}

int* run_instruction(string_int_state_t** s, struct instruction* i){
    switch(i->type){
        case IBLOCK: {
            int* r;
            for (int j = 0; j < list_length(i->iblock.l); j++) {
                r = run_instruction(s, list_nth(i->iblock.l, j));
            }
            return r;
        }
        case IASSIGN: {
            *s = string_int_set_val(*s, i->iassign.var, run_expression(*s, i->iassign.e));
            return NULL;
        }
        case IPRINT: {
            printf("%d\n", run_expression(*s, i->iprint.e));
            return NULL;
        }
        case IIFTHENELSE: {
            if (run_expression(*s, i->iif.cmp))
                return run_instruction(s, i->iif.ithen);
            else
                return run_instruction(s, i->iif.ielse);
        }
        case IWHILE: {
            int *r;
            while (run_expression(*s, i->iwhile.cmp))
                r = run_instruction(s, i->iwhile.i);
            return r;
        }
        case IRETURN: {
            //printf("res : %d\n", run_expression(*s, i->ireturn.e));
            int r = run_expression(*s, i->ireturn.e);
            int* r2 = some(&r);
            *r2 = r;
            return r2;
        }
        default: {
            printf("run_instruction: unexpected instruction type: %d", i->type);
            exit(1);
        }
    }
}

int run_eprog(struct eprog* p, struct list* args){
  if(!p){
    printf("run_eprog: NULL eprog\n");
    exit(1);
  }
  string_int_state_t* s = NULL;
  // Construction de l'état initial
  for (int i = 0; i < list_length(p->args); i++) {
    s = string_int_set_val(s, list_nth(p->args, i), list_nth_int(args, i));
  }

  // Appel de run_instruction et retour de la valeur de retour
  int* i = run_instruction(&s, p->body);
  //printf("%p\n", i);
  terminate_string_int_state(s);
  return *i;
}
