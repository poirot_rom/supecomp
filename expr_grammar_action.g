tokens SYM_EOF SYM_IDENTIFIER SYM_INTEGER SYM_SEMICOLON SYM_IF SYM_ELSE SYM_PLUS SYM_MINUS SYM_ASTERISK SYM_DIV SYM_EQUALITY SYM_ASSIGN SYM_LPARENTHESIS SYM_RPARENTHESIS SYM_LBRACE SYM_RBRACE SYM_WHILE SYM_RETURN SYM_COMMA SYM_LT SYM_LEQ SYM_GT SYM_GEQ SYM_NOTEQ SYM_MOD SYM_PRINT
non-terminals<struct ast_node *> S IDENTIFIER INTEGER FACTOR TERM CMP ARG EXPR FARGS INSTRUCTION FUNCTION ASSIGN RETURN PRINT IFBLOCK WHILEBLOCK BLOCK ELSEBLOCK
non-terminals<struct list *> ARGS CMPS TERMS FACTORS INSTRUCTIONS
rules
S -> FUNCTION SYM_EOF { return $1; }
FUNCTION -> IDENTIFIER SYM_LPARENTHESIS FARGS SYM_RPARENTHESIS BLOCK { return make_node(AST_NODE_FUNDEF, cons($1, cons($3, cons(make_node(AST_NODE_FUNBODY, cons($5, NULL)), NULL)))); }
FARGS -> ARG ARGS { return make_node(AST_NODE_FUNARGS, cons($1, $2)); }
FARGS -> { return make_node(AST_NODE_FUNARGS, NULL); }
ARG -> IDENTIFIER { return $1; }
ARGS -> SYM_COMMA ARG ARGS { return cons($2, $3); }
ARGS ->
IDENTIFIER -> SYM_IDENTIFIER { return make_string_leaf(strdup($1)); }
INTEGER -> SYM_INTEGER { return make_int_leaf(atoi($1)); }

EXPR -> CMP CMPS { return make_node(AST_EQS, cons($1, $2)); }

CMP -> TERM TERMS { return make_node(AST_CMPS, cons($1, $2)); }
CMPS -> SYM_EQUALITY CMP CMPS { return cons(make_node(AST_CEQ, NULL), cons($2, $3)); }
CMPS -> SYM_LT CMP CMPS { return cons(make_node(AST_CLT, NULL), cons($2, $3)); }
CMPS -> SYM_GT CMP CMPS { return cons(make_node(AST_CGT, NULL), cons($2, $3)); }
CMPS -> SYM_LEQ CMP CMPS { return cons(make_node(AST_CLE, NULL), cons($2, $3)); }
CMPS -> SYM_GEQ CMP CMPS { return cons(make_node(AST_CGE, NULL), cons($2, $3)); }
CMPS -> SYM_NOTEQ CMP CMPS { return cons(make_node(AST_CNEQ, NULL), cons($2, $3)); }
CMPS ->

TERM -> FACTOR FACTORS { return make_node(AST_TERMS, cons($1, $2)); }
TERMS -> SYM_PLUS TERM TERMS { return cons(make_node(AST_EADD, NULL), cons($2, $3)); }
TERMS -> SYM_MINUS TERM TERMS { return cons(make_node(AST_ESUB, NULL), cons($2, $3)); }
TERMS ->

FACTOR -> IDENTIFIER { return $1; }
FACTOR -> INTEGER { return $1; }
FACTOR -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS { return $2; }
FACTOR -> SYM_MINUS INTEGER { return make_node(AST_ENEG, cons($2, NULL)); }
FACTORS -> SYM_ASTERISK FACTOR FACTORS { return cons(make_node(AST_EMUL, NULL), cons($2, $3)); }
FACTORS -> SYM_DIV FACTOR FACTORS { return cons(make_node(AST_EDIV, NULL), cons($2, $3)); }
FACTORS -> SYM_MOD FACTOR FACTORS { return cons(make_node(AST_EMOD, NULL), cons($2, $3)); }
FACTORS ->

ASSIGN -> IDENTIFIER SYM_ASSIGN EXPR SYM_SEMICOLON { return make_node(AST_IASSIGN, cons($1, cons($3, NULL))); }
RETURN -> SYM_RETURN EXPR SYM_SEMICOLON { return make_node(AST_IRETURN, cons($2, NULL)); }
PRINT -> SYM_PRINT EXPR SYM_SEMICOLON { return make_node(AST_IPRINT, cons($2, NULL)); }
INSTRUCTION -> ASSIGN { return $1; }
INSTRUCTION -> PRINT { return $1; }
INSTRUCTION -> RETURN { return $1; }
INSTRUCTION -> IFBLOCK { return $1; }
INSTRUCTION -> WHILEBLOCK { return $1; }
BLOCK -> SYM_LBRACE INSTRUCTIONS SYM_RBRACE { return make_node(AST_IBLOCK, $2); }
INSTRUCTIONS -> INSTRUCTION INSTRUCTIONS { return cons($1, $2); }
INSTRUCTIONS ->
IFBLOCK -> SYM_IF SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS BLOCK ELSEBLOCK { return make_node(AST_IIFTHENELSE, cons($3, cons($5, cons($6, NULL)))); }
ELSEBLOCK -> SYM_ELSE BLOCK { return $2; }
ELSEBLOCK -> { return make_node(AST_IBLOCK, NULL); }
WHILEBLOCK -> SYM_WHILE SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS BLOCK { return make_node(AST_IWHILE, cons($3, cons($5, NULL))); }
